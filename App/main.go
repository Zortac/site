package main // import "gitlab.com/zortac/site/App"

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/haisum/recaptcha"
)

type ProcessInfo struct {
	Debug      bool
	TlsCert    string
	TlsKey     string
	Domain     string
	DbUser     string
	DbPass     string
	DbName     string
	Recaptcha  recaptcha.R
	MailUser   string
	MailPass   string
	MailServer string
	MailPort   int
}

var pInfo ProcessInfo

func main() {
	pInfo.Debug = GetEnvBool("DEBUG", false)
	pInfo.TlsCert = GetEnvString("TLS_CERT", "")
	pInfo.TlsKey = GetEnvString("TLS_KEY", "")
	pInfo.Domain = GetEnvString("DOMAIN", "localhost")
	pInfo.DbUser = GetEnvString("MONGO_USER", "")
	pInfo.DbPass = GetEnvString("MONGO_PW", "")
	pInfo.DbName = GetEnvString("MONGO_DB", "")
	pInfo.Recaptcha = recaptcha.R{
		Secret: GetEnvString("CAPTCHA_SECRET", ""),
	}

	pInfo.MailUser = GetEnvString("MAIL_USER", "")
	pInfo.MailPass = GetEnvString("MAIL_PASS", "")
	pInfo.MailServer = GetEnvString("MAIL_SERVER", "")
	pInfo.MailPort = GetEnvInt("MAIL_PORT", 0)

	InitLogging()

	log.Info("Starting up...")

	InitDB()

	InitSessions()

	AddRoute("/", HomeHandler)                                  // Dynamic (Finished)
	AddRoute("/blog", BlogHandler)                              // Dynamic (Finished)
	AddRoute("/listposts", ListPostsHandler)                    // Dynamic (Finished)
	AddRoute("/post/edit/{postid}", PostEditHandler)            // Dynamic (Finished)
	AddRoute("/post/delete/{postid}", PostDeletePageHandler)    // Dynamic (Finished)
	AddRoute("/post/{postid}", PostHandler)                     // Dynamic (Finished)
	AddRoute("/login", LoginPageHandler)                        // Static (Finished)
	AddRoute("/about", AboutHandler)                            // Static (Finished)
	AddRoute("/work", WorkHandler)                              // Static (Finished)
	AddRoute("/privacy", PrivacyHandler)                        // Static (Finished)
	AddRoute("/legal", LegalHandler)                            // Static (Finished)
	AddRoute("/api/login", LoginHandler)                        // API (Finished)
	AddRoute("/api/users/add", UseraddHandler)                  // API (Finished)
	AddRoute("/api/users/delete", UserdelHandler)               // API (Finished)
	AddRoute("/api/posts/update", PostCreateOrUpdateHandler)    // API (Finished)
	AddRoute("/api/posts/delete", PostDeleteHandler)            // API (Finished)
	AddRoute("/api/posts/togglev/{postid}", PostVToggleHandler) // API
	AddRoute("/api/message", MessageHandler)                    // API (Finished)

	AddSubrouter(func(r *mux.Router) {
		r.PathPrefix("/resources/").Handler(http.StripPrefix("/resources/", http.FileServer(http.Dir("static/resources"))))
		log.Info("Added subrouter for /resources/ to serve static files.")
	})

	log.Fatal(RunServer())
}
