package main

import (
	"crypto/tls"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/unrolled/secure"
)

var routes map[string]func(http.ResponseWriter, *http.Request)
var subrouters []func(*mux.Router)

func AddRoute(name string, handler func(http.ResponseWriter, *http.Request)) {
	if routes == nil {
		routes = make(map[string]func(http.ResponseWriter, *http.Request))
	}

	if routes[name] != nil {
		log.Warning("Route", name, "had been added multiple times. It will only be registered once.")
	}

	routes[name] = handler

	log.Info("Added route", name)
}

func AddSubrouter(rAdder func(*mux.Router)) {
	if subrouters == nil {
		subrouters = make([]func(*mux.Router), 0)
	}

	subrouters = append(subrouters, rAdder)
}

func RunServer() error {
	log.Info("Initializing new router...")
	r := mux.NewRouter()

	for route, handler := range routes {
		r.HandleFunc(route, handler)
		log.Info("Registered handler for", route)
	}

	for _, rAdder := range subrouters {
		rAdder(r)
	}

	log.Info("Router initialization complete.")

	log.Info("Inititalizing HTTP server...")

	secureMiddleware := secure.New(secure.Options{
		STSSeconds:            31536000,
		STSIncludeSubdomains:  true,
		STSPreload:            true,
		FrameDeny:             true,
		ContentTypeNosniff:    true,
		BrowserXssFilter:      true,
		ContentSecurityPolicy: globalCSP,
	})

	tlsCfg := &tls.Config{
		PreferServerCipherSuites: true,
		CurvePreferences: []tls.CurveID{
			tls.CurveP256,
			tls.X25519,
		},
		MinVersion: tls.VersionTLS12,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
		},
	}

	server := &http.Server{
		Addr:           ":8080",
		Handler:        secureMiddleware.Handler(r),
		ReadTimeout:    5 * time.Second,
		WriteTimeout:   5 * time.Second,
		IdleTimeout:    120 * time.Second,
		MaxHeaderBytes: 1 << 20,
		TLSConfig:      tlsCfg,
	}

	// Run server, w/ TLS if possible. If no TLS possible, warn and run without.
	if pInfo.TlsCert == "" || pInfo.TlsKey == "" {
		log.Warning("No TLS key/cert specified, running without TLS enabled")
		log.Info("Starting HTTP server...")
		return server.ListenAndServe()
	}

	log.Info("TLS enabled, starting HTTPS server...")
	return server.ListenAndServeTLS(pInfo.TlsCert, pInfo.TlsKey)
}
