package main

import (
	"encoding/json"
	"html/template"
	"strconv"
	"strings"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/russross/blackfriday"
)

type Post struct {
	Id        bson.ObjectId `bson:"_id,omitempty"`
	Author    string        `bson:"author"`
	Hidden    bool          `bson:"hidden"`
	Timestamp int64         `bson:"timestamp"`
	Title     string        `bson:"title"`
	Abstract  string        `bson:"abstract"`
	Content   string        `bson:"content"`
	ImagePath string        `bson:"imageloc"`
	Edit      bool
}

func GetFrontpageContent() template.HTML {
	var posts []Post
	// Get 7 newest visible posts
	DB.C("posts").Find(&bson.M{"hidden": false}).Sort("-timestamp").Limit(7).All(&posts)

	if len(posts) > 0 {
		var b strings.Builder

		// Insert newest, highlighted post into builder
		highlTemplate, err := template.ParseFiles("static/templates/frontpage/highlighted.gohtml")
		if err != nil {
			log.Error("Could not open template file:", err)
			return template.HTML("")
		}

		err = highlTemplate.Execute(&b, posts[0])
		if err != nil {
			log.Error("Error during template execution:", err)
			json, _ := json.Marshal(posts[0])
			log.Error(string(json))
			return template.HTML("")
		}

		// Begin list of older posts
		b.WriteString("<section class=\"posts\">")

		for _, post := range posts[1:] {
			tmpl, err := template.ParseFiles("static/templates/frontpage/standard.gohtml")
			if err != nil {
				log.Error("Could not open template file:", err)
			}

			err = tmpl.Execute(&b, post)
			if err != nil {
				log.Error("Error during template execution:", err)
			}
		}

		// End list of older posts
		b.WriteString("</section>")

		return template.HTML(b.String())
	} else {
		return template.HTML("")
	}
}

func GetPostContent(id string) template.HTML {
	if bson.IsObjectIdHex(id) {
		var post Post
		var b strings.Builder

		err := DB.C("posts").FindId(bson.ObjectIdHex(id)).One(&post)

		if err != nil {
			log.Warning("Could not retreive requested post from database:", err)
			return template.HTML("The requested post " + id + " was not found on this server.")
		}

		tmpl, err := template.ParseFiles("static/templates/post.gohtml")
		if err != nil {
			log.Error("Could not open template file:", err)
			return template.HTML("")
		}

		err = tmpl.Execute(&b, post)
		if err != nil {
			log.Error("Error during template execution:", err)
			return template.HTML("")
		}

		return template.HTML(b.String())

	} else {
		return template.HTML("The requested post " + id + " was not found on this server.")
	}
}

func GetEditPostContent(id string) template.HTML {
	if bson.IsObjectIdHex(id) || id == "new" {
		var post Post
		var b strings.Builder

		if bson.IsObjectIdHex(id) {
			err := DB.C("posts").FindId(bson.ObjectIdHex(id)).One(&post)

			if err != nil {
				log.Warning("Could not retreive requested post from database:", err)
				return template.HTML("The requested post " + id + " was not found on this server.")
			}

			post.Edit = true
		}

		tmpl, err := template.ParseFiles("static/templates/admin/editpost.gohtml")
		if err != nil {
			log.Error("Could not open template file:", err)
			return template.HTML("")
		}

		err = tmpl.Execute(&b, post)
		if err != nil {
			log.Error("Error during template execution:", err)
			return template.HTML("")
		}

		return template.HTML(b.String())

	} else {
		return template.HTML("The requested post " + id + " was not found on this server.")
	}
}

func GetPostList() template.HTML {
	var posts []Post
	var b strings.Builder

	DB.C("posts").Find(&bson.M{}).Sort("-timestamp").All(&posts)

	tmpl, err := template.ParseFiles("static/templates/admin/postlist.gohtml")
	if err != nil {
		log.Error("Could not open template file:", err)
		return template.HTML("")
	}

	err = tmpl.Execute(&b, posts)
	if err != nil {
		log.Error("Error during template execution:", err)
		return template.HTML("")
	}

	return template.HTML(b.String())
}

func GetDeletePostContent(id string) template.HTML {
	if bson.IsObjectIdHex(id) {
		var post Post
		var b strings.Builder

		err := DB.C("posts").FindId(bson.ObjectIdHex(id)).One(&post)

		if err != nil {
			log.Warning("Could not retreive requested post from database:", err)
			return template.HTML("The requested post " + id + " was not found on this server.")
		}

		tmpl, err := template.ParseFiles("static/templates/admin/deletepost.gohtml")
		if err != nil {
			log.Error("Could not open template file:", err)
			return template.HTML("")
		}

		err = tmpl.Execute(&b, post)
		if err != nil {
			log.Error("Error during template execution:", err)
			return template.HTML("")
		}

		return template.HTML(b.String())

	} else {
		return template.HTML("The requested post " + id + " was not found on this server.")
	}
}

func (p Post) Date() string {
	return time.Unix(p.Timestamp, 0).Format("Jan 2, 2006")
}

func (p Post) HexId() string {
	return p.Id.Hex()
}

func (p Post) TitleBreaks(n int) template.HTML {
	return InsertLineBreaks(p.Title, n)
}

func (p Post) AbstractBreaks(n int) template.HTML {
	return InsertLineBreaks(p.Abstract, n)
}

func (p Post) ContentHTML() template.HTML {
	htmlString := string(blackfriday.Run([]byte(p.Content), blackfriday.WithExtensions(
		blackfriday.Tables|blackfriday.FencedCode|blackfriday.Strikethrough|
			blackfriday.Footnotes|blackfriday.NoEmptyLineBeforeBlock|blackfriday.DefinitionLists)))

	// Increase heading level
	for l := 5; l > 0; l-- {
		currentLevel := "h" + strconv.Itoa(l) + ">"
		nextLevel := "h" + strconv.Itoa(l+1) + ">"
		htmlString = strings.Replace(htmlString, currentLevel, nextLevel, -1)
	}
	return template.HTML(htmlString)

}

func InsertLineBreaks(text string, n int) template.HTML {
	words := strings.Split(text, " ")
	var out strings.Builder

	for i, word := range words {
		out.WriteString(word)
		out.WriteString(" ")
		if (i+1)%n == 0 {
			out.WriteString("<br />")
		}
	}

	return template.HTML(out.String())
}
