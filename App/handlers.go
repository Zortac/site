package main

import (
	"html/template"
	"net/http"

	"github.com/gorilla/mux"
)

var SiteList []Site = []Site{
	Site{
		Href: "/",
		Name: "Home",
	},
	Site{
		Href: "/about",
		Name: "About",
	},
	Site{
		Href: "/work",
		Name: "Work",
	},
	Site{
		Href: "/blog",
		Name: "Blog",
	},
	Site{
		Href:      "/listposts",
		Name:      "Posts",
		AdminOnly: true,
	},
}

func HomeHandler(w http.ResponseWriter, r *http.Request) {
	role := VerifySession(w, r)
	baseTmpl, err := template.ParseFiles("static/templates/base.gohtml")
	if err != nil {
		log.Error("Could not parse template file.", err)
		http.Error(w, "Could not parse template file.", http.StatusInternalServerError)
		return
	}

	baseContent := PrepareContent(true, (role != ROLE_NONE), SiteList, "/", "", GetFrontpageContent)

	err = baseTmpl.Execute(w, &baseContent)
	if err != nil {
		log.Error("Could not execute template file.", err)
		http.Error(w, "Could not execute template file.", http.StatusInternalServerError)
		return
	}
}

func BlogHandler(w http.ResponseWriter, r *http.Request) {

}

func PostHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	role := VerifySession(w, r)
	baseTmpl, err := template.ParseFiles("static/templates/base.gohtml")
	if err != nil {
		log.Error("Could not parse template file.", err)
		http.Error(w, "Could not parse template file.", http.StatusInternalServerError)
		return
	}

	baseContent := PrepareContent(false, (role != ROLE_NONE), SiteList, "/blog", "Blog", func() template.HTML {
		return GetPostContent(vars["postid"])
	})

	err = baseTmpl.Execute(w, &baseContent)
	if err != nil {
		log.Error("Could not execute template file.", err)
		http.Error(w, "Could not execute template file.", http.StatusInternalServerError)
		return
	}
}

func AboutHandler(w http.ResponseWriter, r *http.Request) {
	ServeStaticHTML("static/templates/about.html", "/about", "About", SiteList, w, r)
}

func WorkHandler(w http.ResponseWriter, r *http.Request) {
	ServeStaticHTML("static/templates/work.html", "/work", "Work", SiteList, w, r)
}

func PrivacyHandler(w http.ResponseWriter, r *http.Request) {
	ServeStaticHTML("static/templates/privacy.html", "", "Datenschutz", SiteList, w, r)
}

func LegalHandler(w http.ResponseWriter, r *http.Request) {
	ServeStaticHTML("static/templates/legal.html", "", "Impressum", SiteList, w, r)
}

func LoginPageHandler(w http.ResponseWriter, r *http.Request) {
	role := VerifySession(w, r)

	if role != ROLE_NONE {
		// Already logged in, serve log out page
		ServeStaticHTML("static/templates/logout.html", "", "Sign Out", SiteList, w, r)
	} else {
		ServeStaticHTML("static/templates/login.html", "", "Sign In", SiteList, w, r)
	}
}

func PostEditHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	role := VerifySession(w, r)

	if role != ROLE_ADMIN && role != ROLE_AUTHOR {
		http.Error(w, "You must be logged in to create or edit posts.", http.StatusUnauthorized)
		return
	}

	baseTmpl, err := template.ParseFiles("static/templates/base.gohtml")
	if err != nil {
		log.Error("Could not parse template file.", err)
		http.Error(w, "Could not parse template file.", http.StatusInternalServerError)
		return
	}

	baseContent := PrepareContent(false, (role != ROLE_NONE), SiteList, "/listposts", "Edit Post", func() template.HTML {
		return GetEditPostContent(vars["postid"])
	})

	err = baseTmpl.Execute(w, &baseContent)
	if err != nil {
		log.Error("Could not execute template file.", err)
		http.Error(w, "Could not execute template file.", http.StatusInternalServerError)
		return
	}
}

func ListPostsHandler(w http.ResponseWriter, r *http.Request) {
	role := VerifySession(w, r)

	if role != ROLE_ADMIN && role != ROLE_AUTHOR {
		http.Error(w, "You must be logged in to view all posts.", http.StatusUnauthorized)
		return
	}

	baseTmpl, err := template.ParseFiles("static/templates/base.gohtml")
	if err != nil {
		log.Error("Could not parse template file.", err)
		http.Error(w, "Could not parse template file.", http.StatusInternalServerError)
		return
	}

	baseContent := PrepareContent(false, (role != ROLE_NONE), SiteList, "/listposts", "List of Posts", GetPostList)

	err = baseTmpl.Execute(w, &baseContent)
	if err != nil {
		log.Error("Could not execute template file.", err)
		http.Error(w, "Could not execute template file.", http.StatusInternalServerError)
		return
	}
}

func PostDeletePageHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	role := VerifySession(w, r)

	if role != ROLE_ADMIN && role != ROLE_AUTHOR {
		http.Error(w, "You must be logged in to delete posts.", http.StatusUnauthorized)
		return
	}

	baseTmpl, err := template.ParseFiles("static/templates/base.gohtml")
	if err != nil {
		log.Error("Could not parse template file.", err)
		http.Error(w, "Could not parse template file.", http.StatusInternalServerError)
		return
	}

	baseContent := PrepareContent(false, (role != ROLE_NONE), SiteList, "/listposts", "Edit Post", func() template.HTML {
		return GetDeletePostContent(vars["postid"])
	})

	err = baseTmpl.Execute(w, &baseContent)
	if err != nil {
		log.Error("Could not execute template file.", err)
		http.Error(w, "Could not execute template file.", http.StatusInternalServerError)
		return
	}
}
