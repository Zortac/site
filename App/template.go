package main

import (
	"html/template"
	"io/ioutil"
	"net/http"
)

type Site struct {
	Href      string
	Name      string
	AdminOnly bool
}

type PageLink struct {
	IsActive bool
	S        Site
}

type BaseContent struct {
	IsFrontpage    bool
	IsLoggedIn     bool
	NavLinks       []PageLink
	Title          string
	GetMainContent func() template.HTML
}

func (l *PageLink) GetListRepresentation(loggedIn bool) template.HTML {

	show := (!l.S.AdminOnly || loggedIn)

	if !show {
		return template.HTML("")
	}

	if l.IsActive {
		return template.HTML("<li class=\"active\"><a href=\"" + l.S.Href + "\">" + l.S.Name + "</a></li>")
	} else {
		return template.HTML("<li><a href=\"" + l.S.Href + "\">" + l.S.Name + "</a></li>")
	}
}

func PrepareContent(isFp, isLoggedIn bool, sites []Site, activeSite, title string, mainContentProvider func() template.HTML) *BaseContent {
	nLinks := make([]PageLink, 0)

	for _, site := range sites {
		nLinks = append(nLinks, PageLink{
			IsActive: (site.Href == activeSite),
			S:        site,
		})
	}

	return &BaseContent{
		IsFrontpage:    isFp,
		IsLoggedIn:     isLoggedIn,
		NavLinks:       nLinks,
		Title:          title,
		GetMainContent: mainContentProvider,
	}
}

func ServeStaticHTML(path, activeSite, title string, sites []Site, w http.ResponseWriter, r *http.Request) {
	loggedIn := (VerifySession(w, r) != ROLE_NONE)
	baseTmpl, err := template.ParseFiles("static/templates/base.gohtml")
	if err != nil {
		log.Error("Could not parse template file.", err)
		http.Error(w, "Could not parse template file.", http.StatusInternalServerError)
		return
	}

	baseContent := PrepareContent(false, loggedIn, sites, activeSite, title, func() template.HTML {
		file, err := ioutil.ReadFile(path)
		if err != nil {
			log.Error("Could not read file:", err)
		}
		return template.HTML(file)
	})

	baseTmpl.Execute(w, &baseContent)
}
