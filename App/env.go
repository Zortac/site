package main

import (
	"os"
	"strconv"
)

func GetEnvBool(name string, def bool) bool {
	rawenv := os.Getenv(name)
	out, err := strconv.ParseBool(rawenv)
	if err != nil {
		return def
	}

	return out
}

func GetEnvInt(name string, def int) int {
	rawenv := os.Getenv(name)
	out, err := strconv.ParseInt(rawenv, 0, 32)

	if err != nil {
		return def
	}

	return int(out)
}

func GetEnvString(name string, def string) string {
	rawenv := os.Getenv(name)
	if rawenv == "" {
		return def
	}
	return rawenv
}
