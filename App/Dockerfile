# Build SASS -> CSS
FROM ruby:latest AS sass_compile

RUN gem install sass

WORKDIR /sass/src
COPY static/resources/sass sassf

RUN sass --update sassf:css

# Collect static files
FROM alpine:latest AS static_source

WORKDIR /static

COPY static/resources/images ./resources/images
COPY static/resources/dl ./resources/dl
COPY static/resources/js ./resources/js
COPY static/templates ./templates

# Collect dependencies for go compilation
FROM golang:1.11-alpine AS build_base

RUN apk add bash git gcc g++ libc-dev ca-certificates
WORKDIR /go/src/gitlab.com/zortac/site/App

ENV GO111MODULE=on

COPY go.mod .
COPY go.sum .

RUN go mod download

# Compile the app
FROM build_base AS app_builder

COPY *.go ./

RUN go install -v .

# Create final image: Collect everything and prepare to run
FROM alpine AS site_server
WORKDIR /site

RUN apk add ca-certificates

COPY --from=app_builder /go/bin/App /site/App
COPY --from=sass_compile /sass/src/css /site/static/resources/css
COPY --from=static_source /static /site/static

EXPOSE 8080

CMD ["./App"]
