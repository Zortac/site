module gitlab.com/zortac/site/App

require (
	github.com/codegangsta/negroni v1.0.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/mux v1.6.2
	github.com/haisum/recaptcha v0.0.0-20170327142240-7d3b8053900e
	github.com/kr/pretty v0.1.0 // indirect
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/russross/blackfriday v2.0.0+incompatible
	github.com/segmentio/ksuid v1.0.2
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/unrolled/secure v0.0.0-20181221173256-0d6b5bb13069
	golang.org/x/crypto v0.0.0-20181203042331-505ab145d0a9
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
